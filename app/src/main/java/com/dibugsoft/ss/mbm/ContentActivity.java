package com.dibugsoft.ss.mbm;

import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;

import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;

public class ContentActivity extends AppCompatActivity {
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_item_details);

        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        String itemDetails = "";
        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            itemDetails = extras.getString("itemDetails");
        }
        Item item = new Gson().fromJson(itemDetails, Item.class);

        TextView itemTitle = findViewById(R.id.itemTitle);
        itemTitle.setText(item.getTitle());

        ImageView itemImage = findViewById(R.id.itemImage);
        Ion.with(itemImage).placeholder(android.R.drawable.picture_frame)
                .error(R.drawable.madan_bhandari).load(item.getThumbnail());

        TextView itemDetail = findViewById(R.id.itemDetail);
        itemDetail.setText(item.getDetails());

        TextView itemShortNote = findViewById(R.id.itemShortNote);
        itemShortNote.setText(item.getShortNote());
    }

    @Override
    public boolean onSupportNavigateUp() {
        finish();
        return true;
    }
}
