package com.dibugsoft.ss.mbm;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.SubMenu;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.view.GravityCompat;
import androidx.drawerlayout.widget.DrawerLayout;
import androidx.recyclerview.widget.GridLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.google.android.material.navigation.NavigationView;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class ItemsActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener {

    private String itemsAPIUrl = "";
    private String subCategoryAPIUrl = Constants.subCategoryUrl;

    private ArrayList<SubCategory> subCategories;
    private NavigationView navigationView;
    private int categoryID;
    private String categoryTitle, categoryImage;
    private ArrayList<Item> items;
    private TextView textViewTitle;

    private ItemAdapter mItemAdapter;
    private RecyclerView mRecyclerView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_items);
        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        /*FloatingActionButton fab = findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });*/

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.addDrawerListener(toggle);
        toggle.syncState();

        navigationView = findViewById(R.id.nav_view);
        navigationView.setItemIconTintList(null);
        navigationView.setNavigationItemSelectedListener(this);

        textViewTitle = findViewById(R.id.title);

        Bundle extras = getIntent().getExtras();
        if (extras != null) {
            categoryID = extras.getInt("id");
            categoryTitle = extras.getString("title");
            categoryImage = extras.getString("image");
            textViewTitle.setText(categoryTitle);
        }

        mRecyclerView = findViewById(R.id.recyclerViewItems);
        GridLayoutManager mGridLayoutManager = new GridLayoutManager(ItemsActivity.this, 2);
        mRecyclerView.setLayoutManager(mGridLayoutManager);

//        new PopulateSubCategory().execute(subCategoryAPIUrl + "&categoryID=" + categoryID);
        new PopulateSubCategory().execute(subCategoryAPIUrl + categoryID);
//        itemsAPIUrl = Constants.itemUrl + "&categoryID=" + categoryID;
        itemsAPIUrl = Constants.categoryItemUrl + categoryID;
        new PopulateItems().execute(itemsAPIUrl);
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
//        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_login) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

    @SuppressWarnings("StatementWithEmptyBody")
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        // Handle navigation view item clicks here.
        int itemID = item.getItemId();
        if (itemID == R.id.nav_home) {
            Intent intent = new Intent(getBaseContext(), HomeActivity.class);
            startActivity(intent);
        } else {
            try {
                textViewTitle.setText((item.getTitle().toString()));
                int subCategoryID = 0;
                for (SubCategory subCategory : subCategories) {
                    if (subCategory.getTitle().equals(item.getTitle().toString())) {
                        subCategoryID = subCategory.getId();
                        break;
                    }
                }
//                itemsAPIUrl = Constants.itemUrl + "&subCategoryID=" + subCategoryID;
                itemsAPIUrl = Constants.subCategoryItemUrl + subCategoryID;
//                Toast.makeText(getBaseContext(), itemsAPIUrl, Toast.LENGTH_SHORT).show();
            } catch (Exception e) {
                e.printStackTrace();
            }
            new PopulateItems().execute(itemsAPIUrl);
        }

        DrawerLayout drawer = findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    private class PopulateItems extends
            AsyncTask<String, Void, Void> {

        @Override
        public void onPreExecute() {
            Toast.makeText(getBaseContext(), "Loading... Please wait.", Toast.LENGTH_LONG).show();
        }

        @Override
        protected Void doInBackground(String... params) {
            StringBuilder builder = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url.openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                if (statusCode == 200) {
                    InputStream content = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                } else {
                    Log.e(ItemsActivity.class.toString(),
                            "Failed to download file");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                int id = 0;
                String title = "", thumbnail = "", shortNote = "", details = "", image = "";
                items = new ArrayList<>();
                String jsonString = builder.toString();
                JSONArray jsonArray = new JSONArray(jsonString);
                for (int i = 0; i < jsonArray.length(); i++) {
                    JSONObject jsonObj = jsonArray.getJSONObject(i);
                    if (jsonObj.has("id"))
                        id = jsonObj.getInt("id");
                    if (jsonObj.has("title"))
                        title = jsonObj.getString("title");
                    if (jsonObj.has("thumbnail"))
                        thumbnail = jsonObj.getString("thumbnail");
                    if (jsonObj.has("image"))
                        image = jsonObj.getString("image");
                    if (jsonObj.has("shortNote"))
                        shortNote = jsonObj.getString("shortNote");
                    if (jsonObj.has("details"))
                        details = jsonObj.getString("details");
                    items.add(new Item(id, title, thumbnail, shortNote, image, details));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            if (items.size() > 0) {
                mItemAdapter = new ItemAdapter(getBaseContext(), items);
                mRecyclerView.setAdapter(mItemAdapter);
            } else {
                Toast.makeText(getBaseContext(), "Oops! Something went wrong. Please try again.",
                        Toast.LENGTH_SHORT).show();
            }
        }
    }

    private class PopulateSubCategory extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            StringBuilder builder = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                if (statusCode == 200) {
                    // HttpEntity entity = response.getEntity();
                    InputStream content = connection.getInputStream(); // entity.getContent();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                } else {
                    Log.e(ItemsActivity.class.toString(),
                            "Failed to download file");
                }
            } catch (Exception ex) {
                Log.e("HTTP Error", ex.getMessage());
            }
            try {
                subCategories = new ArrayList<>();
                int id = 0;
                String title = "", image = "";
                String jsonString = builder.toString();
                JSONArray jsonArray = new JSONArray(jsonString);
                JSONObject jsonObj;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObj = jsonArray.getJSONObject(i);
                    if (jsonObj.has("id")) {
                        id = jsonObj.getInt("id");
                    }
                    if (jsonObj.has("title")) {
                        title = jsonObj.getString("title");
                    }
                    if (jsonObj.has("image"))
                        image = jsonObj.getString(("image"));
                    subCategories.add(new SubCategory(id, title, image));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            Menu menu = navigationView.getMenu();
            SubMenu subMenu = menu.addSubMenu("Sub Category Menu");
            for (int i = 0; i < subCategories.size(); i++) {
                subMenu.add(subCategories.get(i).getTitle());
            }
        }

    }
}
