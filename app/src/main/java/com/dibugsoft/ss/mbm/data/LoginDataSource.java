package com.dibugsoft.ss.mbm.data;

import android.content.Context;
import android.content.SharedPreferences;
import android.util.Log;
import android.widget.Toast;

import com.dibugsoft.ss.mbm.Constants;
import com.dibugsoft.ss.mbm.data.model.LoggedInUser;
import com.google.gson.JsonObject;
import com.koushikdutta.async.future.FutureCallback;
import com.koushikdutta.ion.Ion;
import com.koushikdutta.ion.Response;

import java.io.IOException;

/**
 * Class that handles authentication w/ login credentials and retrieves user information.
 */
public class LoginDataSource {

    private SharedPreferences sharedpreferences;
    LoggedInUser user;

    public Result<LoggedInUser> login(final Context context, String username, String password) {

        try {
            sharedpreferences = context.getSharedPreferences("token", Context.MODE_PRIVATE);

            JsonObject json = new JsonObject();
            json.addProperty("Email", username);
            json.addProperty("Password", password);
            Response<JsonObject> result = Ion.with(context)
                .load(Constants.baseAPIUrl + "admin")
                .setJsonObjectBody(json)
                .asJsonObject()
                .withResponse()
                .get()
                /*.setCallback(new FutureCallback<Response<JsonObject>>() {
                    @Override
                    public void onCompleted(Exception e, Response<JsonObject> result) {
                        if (e != null) {
                            Toast.makeText(context, e.getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            int responseCode = result.getHeaders().code();
                            Log.e("response", String.valueOf(result.getHeaders().code()));
                            if (responseCode != 200) {
                                Toast.makeText(context, "Unable to log in."
                                        , Toast.LENGTH_SHORT).show();
                                return;
                            } else {
                                String token = result.getResult().get("token").getAsString();
                                String name = result.getResult().get("name").getAsString();
                                SharedPreferences.Editor editor = sharedpreferences.edit();
                                editor.putString("token", token);
                                editor.commit();
                                user = new LoggedInUser(token, name);
                            }
                        }
                    }
                })*/;

            /*
            // TODO: handle loggedInUser authentication
            LoggedInUser fakeUser =
                    new LoggedInUser(
                            java.util.UUID.randomUUID().toString(),
                            "Jane Doe");
            */
            int responseCode = result.getHeaders().code();
            Log.e("response", String.valueOf(result.getHeaders().code()));
            if (responseCode != 200) {
                Toast.makeText(context, "Unable to log in."
                        , Toast.LENGTH_SHORT).show();
                return new Result.Error(new Exception("Error logging in"));
            } else {
                String token = result.getResult().get("token").getAsString();
                String name = result.getResult().get("name").getAsString();
                SharedPreferences.Editor editor = sharedpreferences.edit();
                editor.putString("token", token);
                editor.commit();
                user = new LoggedInUser(token, name);
            }
            return new Result.Success<>(user);
        } catch (Exception e) {
            return new Result.Error(new IOException("Error logging in", e));
        }
    }

    public void logout() {
        // TODO: revoke authentication
    }
}
