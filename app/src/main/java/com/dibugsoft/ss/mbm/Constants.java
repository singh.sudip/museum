package com.dibugsoft.ss.mbm;

public class Constants {
    //    public static String baseAPIUrl = "http://202.45.146.62/mbmcrm/api/";
    public static String baseAPIUrl = "http://192.168.100.6:8000/api/";
    //    public static String categoryUrl = baseAPIUrl + "mbm.aspx?q=category";
    public static String categoryUrl = baseAPIUrl + "categories";
    //    public static String subCategoryUrl = baseAPIUrl + "mbm.aspx?q=subCategory";
    public static String subCategoryUrl = baseAPIUrl + "subcategories/";
    public static String itemUrl = baseAPIUrl + "mbm.aspx?q=item";
    public static String categoryItemUrl = baseAPIUrl + "categoryItems/";
    public static String subCategoryItemUrl = baseAPIUrl + "subCategoryItems/";
    public static String areaUrl = baseAPIUrl + "mbm.aspx?q=area";
    public static Boolean isLoggedIn = false;
    public static String loggedInUser = "";
    public static String loggedInUserEmail = "";
}
