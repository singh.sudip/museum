package com.dibugsoft.ss.mbm;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.provider.Settings;
import android.util.Log;

import androidx.core.app.NotificationCompat;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class GPSTracker extends Service implements LocationListener {
    private static ArrayList<Area> mAreaList = new ArrayList<>();
    private static String LOCATION_LOG = "Location Tracker Log";
    private static String lastNotifiedAreaName = "";

    private final Context mContext;

    // flag for GPS status
    boolean isGPSEnabled = false;

    // flag for network status
    boolean isNetworkEnabled = false;

    // flag for GPS status
    boolean canGetLocation = false;

    Location location; // location
    double altitude; // altitude
    double latitude; // latitude
    double longitude; // longitude

    // The minimum distance to change Updates in meters
    private static final long MIN_DISTANCE_CHANGE_FOR_UPDATES = 10; // 10 meters

    // The minimum time between updates in milliseconds
    private static final long MIN_TIME_BW_UPDATES = 1000 * 60 * 1; // 1 minute

    // Declaring a Location Manager
    protected LocationManager locationManager;

    public GPSTracker(Context context) {
        this.mContext = context;
        getLocation();

        new GetAreas().execute(Constants.areaUrl);
    }

    @SuppressLint("MissingPermission")
    public Location getLocation() {
        try {
            locationManager = (LocationManager) mContext.getSystemService(LOCATION_SERVICE);

            // getting GPS status
            isGPSEnabled = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);

            // getting network status
            isNetworkEnabled = locationManager
                    .isProviderEnabled(LocationManager.NETWORK_PROVIDER);

            if (!isGPSEnabled && !isNetworkEnabled) {
                // no network provider is enabled
            } else {
                this.canGetLocation = true;
                // First get location from Network Provider
                if (isNetworkEnabled) {
                    locationManager.requestLocationUpdates(
                            LocationManager.NETWORK_PROVIDER,
                            MIN_TIME_BW_UPDATES,
                            MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                    Log.d("Network", "Network");
                    if (locationManager != null) {
                        location = locationManager
                                .getLastKnownLocation(LocationManager.NETWORK_PROVIDER);

                        if (location != null) {
                            latitude = location.getLatitude();
                            longitude = location.getLongitude();
                        }
                    }
                }

                // if GPS Enabled get lat/long using GPS Services
                if (isGPSEnabled) {
                    if (location == null) {
                        locationManager.requestLocationUpdates(
                                LocationManager.GPS_PROVIDER,
                                MIN_TIME_BW_UPDATES,
                                MIN_DISTANCE_CHANGE_FOR_UPDATES, this);

                        Log.d("GPS Enabled", "GPS Enabled");
                        if (locationManager != null) {
                            location = locationManager
                                    .getLastKnownLocation(LocationManager.GPS_PROVIDER);

                            if (location != null) {
                                latitude = location.getLatitude();
                                longitude = location.getLongitude();
                            }
                        }
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

        return location;
    }

    /**
     * Stop using GPS listener
     * Calling this function will stop using GPS in your app
     */

    public void stopUsingGPS() {
        if (locationManager != null) {
            locationManager.removeUpdates(GPSTracker.this);
        }
    }

    /*
     * Function to get altitude
     */
    public double getAltitude() {
        if (location != null) {
            altitude = location.getAltitude();
        }
        return altitude;
    }

    /**
     * Function to get latitude
     */

    public double getLatitude() {
        if (location != null) {
            latitude = location.getLatitude();
        }

        // return latitude
        return latitude;
    }

    /**
     * Function to get longitude
     */

    public double getLongitude() {
        if (location != null) {
            longitude = location.getLongitude();
        }

        // return longitude
        return longitude;
    }

    /**
     * Function to check GPS/wifi enabled
     *
     * @return boolean
     */

    public boolean canGetLocation() {
        return this.canGetLocation;
    }

    /**
     * Function to show settings alert dialog
     * On pressing Settings button will lauch Settings Options
     */

    public void showSettingsAlert() {
        AlertDialog.Builder alertDialog = new AlertDialog.Builder(mContext);

        // Setting Dialog Title
        alertDialog.setTitle("GPS is settings");

        // Setting Dialog Message
        alertDialog.setMessage("GPS is not enabled. Do you want to go to settings menu?");

        // On pressing Settings button
        alertDialog.setPositiveButton("Settings", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
                mContext.startActivity(intent);
            }
        });

        // on pressing cancel button
        alertDialog.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int which) {
                dialog.cancel();
            }
        });

        // Showing Alert Message
        alertDialog.show();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(LOCATION_LOG, "Location changed.");
        if (location != null && mAreaList.size() > 0) {
            for (Area area : mAreaList) {
                float[] distance = new float[2];
                Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                        area.getCenterLatitude(), area.getCenterLongitude(), distance);

                if (location.hasAltitude())
                    Log.e(getBaseContext().getClass().getName(),
                            "Altitude : " + location.getAltitude());

                if (distance[0] <= area.getRadius()) {
                    if (!lastNotifiedAreaName.equals(area.getAreaName())) {
                        lastNotifiedAreaName = area.getAreaName();
                        createNotification(area);
                    }
                    Log.e(getBaseContext().getClass().getName(), "Inside, distance from center: " +
                            distance[0] + " radius: " + area.getRadius() + " area: " +
                            area.getAreaName());
                }
                if (distance[0] > area.getRadius()) {
                    Log.e(getBaseContext().getClass().getName(), "Outside, distance from center: " +
                            distance[0] + " radius: " + area.getRadius() + " area: " +
                            area.getAreaName());
                } else {
                    Log.e(getBaseContext().getClass().getName(), "Inside, distance from center: " +
                            distance[0] + " radius: " + area.getRadius() + " area: " +
                            area.getAreaName());
                }
            }
        }
    }

    @Override
    public void onProviderDisabled(String provider) {
    }

    @Override
    public void onProviderEnabled(String provider) {
    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {
    }

    @Override
    public IBinder onBind(Intent arg0) {
        return null;
    }


    private class GetAreas extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            StringBuilder builder = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                if (statusCode == 200) {
                    InputStream content = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                } else {
                    Log.e(LOCATION_LOG, "Failed to download file");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                mAreaList = new ArrayList<>();
                int id = 0;
                String code = "", areaName = "", message = "";
                long centerLatitude = 0, centerLongitude = 0, radius = 0;
                String jsonString = builder.toString();
                JSONObject result = new JSONObject(jsonString);
                String dataString = result.getString("data");
                JSONArray jsonArray = new JSONArray(dataString);
                JSONObject jsonObj;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObj = jsonArray.getJSONObject(i);
                    if (jsonObj.has("id")) {
                        id = jsonObj.getInt("id");
                    }
                    if (jsonObj.has("code")) {
                        code = jsonObj.getString("code");
                    }
                    if (jsonObj.has("area_name"))
                        areaName = jsonObj.getString("area_name");
                    if (jsonObj.has("message"))
                        message = jsonObj.getString("message");
                    if (jsonObj.has("center_latitude"))
                        centerLatitude = jsonObj.getLong("center_latitude");
                    if (jsonObj.has("center_longitude"))
                        centerLongitude = jsonObj.getLong("center_longitude");
                    if (jsonObj.has("area_radius"))
                        radius = jsonObj.getLong("area_radius");
                    mAreaList.add(new Area(id, code, areaName, message, centerLatitude,
                            centerLongitude, radius));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            if (mAreaList.size() == 0) {
                Log.e(LOCATION_LOG, "Oops! Something went wrong. Please try again.");
            }
        }

    }

    public void createNotification(Area area) {
        // Build notification
        // Actions are just fake
        Notification notification = new NotificationCompat.Builder(getBaseContext())
                .setContentTitle(area.getAreaName())
                .setContentText(area.getMessage())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(area.getMessage()))
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, notification);

    }
}