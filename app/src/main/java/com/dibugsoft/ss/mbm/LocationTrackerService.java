package com.dibugsoft.ss.mbm;

import android.Manifest;
import android.app.Notification;
import android.app.NotificationManager;
import android.app.Service;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.IBinder;
import android.util.Log;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.core.app.ActivityCompat;
import androidx.core.app.NotificationCompat;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;

public class LocationTrackerService extends Service implements GoogleApiClient.ConnectionCallbacks,
        GoogleApiClient.OnConnectionFailedListener, LocationListener {
    private static ArrayList<Area> mAreaList = new ArrayList<>();
    private Location location;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    private static final long UPDATE_INTERVAL = 5000, FASTEST_INTERVAL = 5000; // = 5 seconds
    private static String LOCATION_LOG = "Location Tracker Log";
    private static String lastNotifiedAreaName = "";

    @Override
    public void onCreate() {
        super.onCreate();
        Log.e(LOCATION_LOG, "Service created.");
        // we build google api client
        googleApiClient = new GoogleApiClient.Builder(getBaseContext()).
                addApi(LocationServices.API).
                addConnectionCallbacks(this).
                addOnConnectionFailedListener(this).build();
        new GetAreas().execute(Constants.areaUrl);
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
//        Toast.makeText(getBaseContext(), "Service started.", Toast.LENGTH_SHORT).show();
        Log.e(LOCATION_LOG, "Service started.");
        googleApiClient.connect();
        return START_STICKY;
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
//        Toast.makeText(getBaseContext(), "connected.", Toast.LENGTH_SHORT).show();

        Log.e(LOCATION_LOG, "Connected.");
        if (ActivityCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            return;
        }

        // Permissions ok, we get last location
        location = LocationServices.getFusedLocationProviderClient(this).getLastLocation().getResult();
        // location = LocationServices.FusedLocationApi.getLastLocation(googleApiClient);

        if (location != null) {
            Log.e("Location", "Latitude : " + location.getLatitude() + ", Longitude : "
                    + location.getLongitude());
        }
        startLocationUpdates();
    }

    private void startLocationUpdates() {
//        Toast.makeText(getBaseContext(), "Location updated.", Toast.LENGTH_SHORT).show();
        if (!googleApiClient.isConnected())
            googleApiClient.connect();
        locationRequest = new LocationRequest();
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
        locationRequest.setInterval(UPDATE_INTERVAL);
        locationRequest.setFastestInterval(FASTEST_INTERVAL);

        if (ActivityCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED
                && ActivityCompat.checkSelfPermission(getBaseContext(),
                Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            Toast.makeText(getBaseContext(), "You need to enable permissions to display location !",
                    Toast.LENGTH_SHORT).show();
        }

        LocationServices.getFusedLocationProviderClient(this).getLastLocation()
            .addOnSuccessListener(new OnSuccessListener<Location>() {
                @Override
                public void onSuccess(Location location) {
                    // Got last known location. In some rare situations, this can be null.
                    if (location != null) {
                        // Logic to handle location object
                    }
                }
            });

        /*LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest,
                this);*/
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {
        Toast.makeText(getBaseContext(), "Connection failed.", Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onLocationChanged(Location location) {
        Log.e(LOCATION_LOG, "Location changed.");
        if (location != null && mAreaList.size() > 0) {
            for (Area area : mAreaList) {
                float[] distance = new float[2];
                Location.distanceBetween(location.getLatitude(), location.getLongitude(),
                        area.getCenterLatitude(), area.getCenterLongitude(), distance);

                if (location.hasAltitude())
                    Log.e(getBaseContext().getClass().getName(),
                            "Altitude : " + location.getAltitude());

                if (distance[0] <= area.getRadius()) {
                    if(!lastNotifiedAreaName.equals(area.getAreaName())) {
                        lastNotifiedAreaName = area.getAreaName();
                        createNotification(area);
                    }
                    Log.e(getBaseContext().getClass().getName(), "Inside, distance from center: " +
                            distance[0] + " radius: " + area.getRadius() + " area: " +
                            area.getAreaName());
                }
                if (distance[0] > area.getRadius()) {
                    Log.e(getBaseContext().getClass().getName(), "Outside, distance from center: " +
                            distance[0] + " radius: " + area.getRadius() + " area: " +
                            area.getAreaName());
                } else {
                    Log.e(getBaseContext().getClass().getName(), "Inside, distance from center: " +
                            distance[0] + " radius: " + area.getRadius() + " area: " +
                            area.getAreaName());
                }
            }
        }
    }

    private class GetAreas extends AsyncTask<String, Void, Void> {

        @Override
        protected Void doInBackground(String... params) {
            StringBuilder builder = new StringBuilder();
            try {
                URL url = new URL(params[0]);
                HttpURLConnection connection = (HttpURLConnection) url
                        .openConnection();
                connection.setRequestMethod("GET");
                connection.connect();
                int statusCode = connection.getResponseCode();
                if (statusCode == 200) {
                    InputStream content = connection.getInputStream();
                    BufferedReader reader = new BufferedReader(
                            new InputStreamReader(content));
                    String line;
                    while ((line = reader.readLine()) != null) {
                        builder.append(line);
                    }
                } else {
                    Log.e(HomeActivity.class.toString(),
                            "Failed to download file");
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
            try {
                mAreaList = new ArrayList<>();
                int id = 0;
                String code = "", areaName = "", message = "";
                long centerLatitude = 0, centerLongitude = 0, radius = 0;
                String jsonString = builder.toString();
                JSONObject result = new JSONObject(jsonString);
                String dataString = result.getString("data");
                JSONArray jsonArray = new JSONArray(dataString);
                JSONObject jsonObj;
                for (int i = 0; i < jsonArray.length(); i++) {
                    jsonObj = jsonArray.getJSONObject(i);
                    if (jsonObj.has("id")) {
                        id = jsonObj.getInt("id");
                    }
                    if (jsonObj.has("code")) {
                        code = jsonObj.getString("code");
                    }
                    if (jsonObj.has("area_name"))
                        areaName = jsonObj.getString("area_name");
                    if (jsonObj.has("message"))
                        message = jsonObj.getString("message");
                    if (jsonObj.has("center_latitude"))
                        centerLatitude = jsonObj.getLong("center_latitude");
                    if (jsonObj.has("center_longitude"))
                        centerLongitude = jsonObj.getLong("center_longitude");
                    if (jsonObj.has("area_radius"))
                        radius = jsonObj.getLong("area_radius");
                    mAreaList.add(new Area(id, code, areaName, message, centerLatitude,
                            centerLongitude, radius));
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            return null;
        }

        @Override
        public void onPostExecute(Void result) {
            if (mAreaList.size() == 0) {
                Toast.makeText(getBaseContext(), "Oops! Something went wrong. Please try again.",
                        Toast.LENGTH_SHORT).show();
            }
        }

    }

    public void createNotification(Area area) {
        // Build notification
        // Actions are just fake
        Notification notification = new NotificationCompat.Builder(getBaseContext())
                .setContentTitle(area.getAreaName())
                .setContentText(area.getMessage())
                .setSmallIcon(R.mipmap.ic_launcher)
                .setDefaults(NotificationCompat.DEFAULT_ALL)
                .setPriority(NotificationCompat.PRIORITY_DEFAULT)
                .setStyle(new NotificationCompat.BigTextStyle().bigText(area.getMessage()))
                .build();
        NotificationManager notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        // hide the notification after its selected
        notification.flags |= Notification.FLAG_AUTO_CANCEL;

        notificationManager.notify(0, notification);

    }

}
