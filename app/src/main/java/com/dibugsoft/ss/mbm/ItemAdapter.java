package com.dibugsoft.ss.mbm;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.google.gson.Gson;
import com.koushikdutta.ion.Ion;

import java.util.List;

public class ItemAdapter extends RecyclerView.Adapter<ItemViewHolder> {

    private Context mContext;
    private List<Item> mItemList;

    ItemAdapter(Context mContext, List<Item> mItemList) {
        this.mContext = mContext;
        this.mItemList = mItemList;
    }

    @NonNull
    @Override
    public ItemViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View mView = LayoutInflater.from(parent.getContext()).inflate(
                R.layout.item_items, parent, false);
        final ItemViewHolder mHolder = new ItemViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                try {
                    /*Snackbar snackbar = Snackbar.make(parent, mItemList.get(
                            mHolder.getAdapterPosition()).getTitle(), Snackbar.LENGTH_SHORT);
                    snackbar.show();*/
                    Intent intent = new Intent(mContext, ContentActivity.class);
                    intent.putExtra("itemDetails", new Gson().toJson(mItemList.get(
                            mHolder.getAdapterPosition())));
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                }
            }
        });
        return mHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull ItemViewHolder holder, int position) {
        Ion.with(holder.mImage).placeholder(android.R.drawable.picture_frame)
                .error(R.drawable.madan_bhandari).load(mItemList.get(position).getThumbnail());

        holder.mTitle.setText(mItemList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return mItemList.size();
    }
}

class ItemViewHolder extends RecyclerView.ViewHolder {
    ImageView mImage;
    TextView mTitle;

    public ItemViewHolder(View itemView) {
        super(itemView);
        mImage = itemView.findViewById(R.id.itemImage);
        mTitle = itemView.findViewById(R.id.itemTitle);
    }
}
