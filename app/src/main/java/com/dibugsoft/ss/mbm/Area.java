package com.dibugsoft.ss.mbm;

public class Area {
    private int id;
    private String code, areaName, message;
    private long centerLatitude, centerLongitude, radius;

    public Area(int id, String code, String areaName, String message, long centerLatitude,
                long centerLongitude, long radius) {
        super();
        this.id = id;
        this.code = code;
        this.areaName = areaName;
        this.message = message;
        this.centerLatitude = centerLatitude;
        this.centerLongitude = centerLongitude;
        this.radius = radius;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getAreaName() {
        return areaName;
    }

    public void setAreaName(String areaName) {
        this.areaName = areaName;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public long getCenterLatitude() {
        return centerLatitude;
    }

    public void setCenterLatitude(long centerLatitude) {
        this.centerLatitude = centerLatitude;
    }

    public long getCenterLongitude() {
        return centerLongitude;
    }

    public void setCenterLongitude(long centerLongitude) {
        this.centerLongitude = centerLongitude;
    }

    public long getRadius() {
        return radius;
    }

    public void setRadius(long radius) {
        this.radius = radius;
    }
}
