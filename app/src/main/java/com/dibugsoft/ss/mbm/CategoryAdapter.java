package com.dibugsoft.ss.mbm;

import android.content.Context;
import android.content.Intent;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import com.koushikdutta.ion.Ion;

import java.util.List;

public class CategoryAdapter extends RecyclerView.Adapter<CategoryViewHolder> {

    private Context mContext;
    private List<Category> mCategoryList;

    CategoryAdapter(Context mContext, List<Category> mCategoryList) {
        this.mContext = mContext;
        this.mCategoryList = mCategoryList;
    }

    @NonNull
    @Override
    public CategoryViewHolder onCreateViewHolder(@NonNull final ViewGroup parent, int viewType) {
        final View mView = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_category,
                parent, false);
        final CategoryViewHolder mHolder = new CategoryViewHolder(mView);
        mView.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View view) {
                try {
//                    Snackbar mSnackbar = Snackbar.make(parent, mCategoryList.get(
//                          mHolder.getAdapterPosition()).getTitle(), Snackbar.LENGTH_SHORT);
//                    mSnackbar.show();

                    Intent intent = new Intent(mContext, ItemsActivity.class);
                    intent.putExtra("id", mCategoryList.get(mHolder.getAdapterPosition()).getId());
                    intent.putExtra("title", mCategoryList.get(mHolder.getAdapterPosition()).getTitle());
                    intent.putExtra("image", mCategoryList.get(mHolder.getAdapterPosition()).getImage());
                    intent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                    mContext.startActivity(intent);
                } catch (Exception e) {
                    Log.e("Error", e.getMessage());
                }
            }
        });
        return mHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull CategoryViewHolder holder, int position) {
        Ion.with(holder.mImage).placeholder(android.R.drawable.picture_frame)
                .error(R.drawable.madan_bhandari).load(mCategoryList.get(position).getImage());

        holder.mTitle.setText(mCategoryList.get(position).getTitle());
    }

    @Override
    public int getItemCount() {
        return mCategoryList.size();
    }
}

class CategoryViewHolder extends RecyclerView.ViewHolder {
    ImageView mImage;
    TextView mTitle;

    public CategoryViewHolder(View itemView) {
        super(itemView);
        mImage = itemView.findViewById(R.id.categoryImage);
        mTitle = itemView.findViewById(R.id.categoryTitle);
    }
}