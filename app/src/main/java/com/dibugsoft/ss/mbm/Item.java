package com.dibugsoft.ss.mbm;

public class Item {
    private int id;
    private String title, thumbnail, shortNote, image, details;

    public Item(int id, String title, String thumbnail, String shortNote, String image,
                String details) {
        super();
        this.id = id;
        this.title = title;
        this.thumbnail = thumbnail;
        this.shortNote = shortNote;
        this.image = image;
        this.details = details;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public String getThumbnail() {
        return thumbnail;
    }

    public void setThumbnail(String thumbnail) {
        this.thumbnail = thumbnail;
    }

    public String getShortNote() {
        return shortNote;
    }

    public void setShortNote(String shortNote) {
        this.shortNote = shortNote;
    }

    public String getDetails() {
        return details;
    }

    public void setDetails(String details) {
        this.details = details;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}
